export interface Env {
  BOT_TOKEN: string;
  CHAT_ID: string;
}

const telegramUrl = "https://api.telegram.org/bot";

function getTelegramURL(token: string) {
  return telegramUrl + token;
}

async function sendMessage(chatId: string, text: string, BOT_TOKEN: string) {
  // Obtenemos la url de telegram que se compone de la url api official + token
  const telegramUrl = getTelegramURL(BOT_TOKEN);

  // Armamos la url a la cual vamos a enviar la petición
  const url = telegramUrl + "/sendMessage?chat_id=" + chatId + "&text=" + text;
  // utilizamos fetch() que es provisto por el worker
  await fetch(url);
}

export default {
  // Recibimos el evento y ejecutamos alguna acción.
  async scheduled(_: Event, env: Env, ctx: ExecutionContext) {
    // Obtenemos las variables de entorno secretas que son inyectadas por el worker
    const chatId = env.CHAT_ID;
    const token = env.BOT_TOKEN;

    // LLamamos a la función que va a enviar un mensaje a nuestro bot de Telegram
    ctx.waitUntil(sendMessage(chatId, "Hola desde un worker", token));
  },
};
